const http = require('http');
const HttpPort = process.env.HTTP_PORT;

console.log(JSON.stringify(process.env, null, 2));

const srv = http.createServer((req, res) => {
  switch(req.url) {
   case '/':
		res.writeHead(200, { 'Content-Type': 'text/html' });
	   res.end('<h1>Hello World</h1><ul><li><a href="/json">JSON Hello World</a></li><li><a href="/plain">Plain txt Hello World</a></li></ul>');
	case '/json':
	  res.writeHead(200, { 'Content-Type': 'application/json' });
	  res.end(JSON.stringify({
        message: 'Hello World'
	  }));
	  break;
    case '/plain':
	  res.writeHead(200, { 'Content-Type': 'text/plain' });
	  res.end('Hello World');
	  break;
    default:
	  res.writeHead(404, { 'Content-Type': 'text/plain' });
	  res.end('404 Not Found');
  }
});

srv.listen(HttpPort, () => {
	console.log(`HTTP server listening on port ${HttpPort}`);
});