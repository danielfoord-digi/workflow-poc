module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'workflow-poc-development',
      script    : 'server.js',
      env: {
		HTTP_PORT: 8080
      }
    }
	
	
  ]
  
};
